import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import "quill/dist/quill.core.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
 
import "./assets/vendor/nucleo/css/nucleo.css"; 
import "./assets/scss/argon-dashboard-pro-react.scss?v1.2.0";

import App from './App';
import reportWebVitals from './reportWebVitals';
import './i18n'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
 
reportWebVitals();
