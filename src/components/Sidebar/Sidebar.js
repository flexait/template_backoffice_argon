import React, { useState } from "react"; 
import { useLocation, Link } from "react-router-dom"; 
import classnames from "classnames"; 
import { PropTypes } from "prop-types"; 
import PerfectScrollbar from "react-perfect-scrollbar"; 
import { Collapse, NavbarBrand, Navbar, NavItem, NavLink, Nav } from "reactstrap";

function Sidebar({ toggleSidenav, sidenavOpen, logo, rtlActive }) { 
  const [state, setState] = useState({});
  const location = useLocation();

  const onMouseEnterSidenav = () => {
    if (!document.body.classList.contains("g-sidenav-pinned")) {
      document.body.classList.add("g-sidenav-show");
    }
  };

  const onMouseLeaveSidenav = () => {
    if (!document.body.classList.contains("g-sidenav-pinned")) {
      document.body.classList.remove("g-sidenav-show");
    }
  };
  
  let navbarBrandProps;
  if (logo && logo.innerLink) {
    navbarBrandProps = {
      to: logo.innerLink,
      tag: Link,
    };
  } else if (logo && logo.outterLink) {
    navbarBrandProps = {
      href: logo.outterLink,
      target: "_blank",
    };
  }

  const scrollBarInner = (
    <div className="scrollbar-inner">
      <div className="sidenav-header d-flex align-items-center">
        {logo ? (
          <NavbarBrand {...navbarBrandProps}>
            <img
              alt={logo.imgAlt}
              className="navbar-brand-img"
              src={logo.imgSrc}
            />
          </NavbarBrand>
        ) : null}
        <div className="ml-auto">
          <div
            className={classnames("sidenav-toggler d-none d-xl-block", {
              active: sidenavOpen,
            })}
            onClick={toggleSidenav}
          >
            <div className="sidenav-toggler-inner">
              <i className="sidenav-toggler-line" />
              <i className="sidenav-toggler-line" />
              <i className="sidenav-toggler-line" />
            </div>
          </div>
        </div>
      </div>
      <div className="navbar-inner">
        <Collapse navbar isOpen={true}> 
          <hr className="my-3" />
          <h6 className="navbar-heading p-0 text-muted">
            <span className="docs-normal">Features</span>
            <span className="docs-mini">F</span>
          </h6>
          <Nav className="mb-md-3" navbar>

            <NavItem> 
              <Link to="/login" component={NavLink}>
                <i className="ni ni-spaceship" />
                <span className="nav-link-text">Login</span>
              </Link>  
            </NavItem> 

          </Nav>
        </Collapse>
      </div>
    </div>
  );
 
  return (
    <Navbar
      className={
        "sidenav navbar-vertical navbar-expand-xs navbar-light bg-white " +
        (rtlActive ? "" : "fixed-left")
      }
      onMouseEnter={onMouseEnterSidenav}
      onMouseLeave={onMouseLeaveSidenav}
    >
      {navigator.platform.indexOf("Win") > -1 ? (
        <PerfectScrollbar>{scrollBarInner}</PerfectScrollbar>
      ) : (
        scrollBarInner
      )}
    </Navbar>
  );
}

Sidebar.defaultProps = { 
  toggleSidenav: () => {},
  sidenavOpen: false,
  rtlActive: false,
};

Sidebar.propTypes = {
  toggleSidenav: PropTypes.func, 
  sidenavOpen: PropTypes.bool, 
  logo: PropTypes.shape({ 
    innerLink: PropTypes.string, 
    outterLink: PropTypes.string, 
    imgSrc: PropTypes.string.isRequired, 
    imgAlt: PropTypes.string.isRequired,
  }),
  rtlActive: PropTypes.bool,
};

export default Sidebar; 